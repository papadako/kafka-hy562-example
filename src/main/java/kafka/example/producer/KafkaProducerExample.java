package kafka.example.producer;

import java.util.Properties;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;    // Used for key
import org.apache.kafka.common.serialization.StringSerializer;  // Used for value

// Some imports
public class KafkaProducerExample {

    private final static String TOPIC = "hy562-topic"; // topic
    private final static String BOOTSTRAP_SERVERS = "localhost:9092"; // These are the servers

    // Create a new producer
    private static Producer<Long, String> createProducer() {
        Properties props = new Properties();    // Use a properties class
        // Populate it with appropriate configs, e.g. servers and key-value types
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        // Used for logging of specific client
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaProducerExample");
        // Implements Kafka serializer class for Kafka for record keys
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                LongSerializer.class.getName());
        // Implements Kafka serializer class for Kafka for record values
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class.getName());
        // Now create the producer
        return new KafkaProducer<>(props);
    }

    // Run the producer and send sendMessageCount records synchronously
    static void runProducer(final int sendMessageCount) throws Exception {
        // Create the producer
        final Producer<Long, String> producer = createProducer();
        long time = System.currentTimeMillis();

        try {
            for (long index = time; index < time + sendMessageCount; index++) {
                // Create a new producer record
                final ProducerRecord<Long, String> record
                        = new ProducerRecord<>(TOPIC, index,
                                "Hello times " + index);

                // Send the record. Returns a java future
                // Record metadata holds partition number and offset
                RecordMetadata metadata = producer.send(record).get();

                long elapsedTime = System.currentTimeMillis() - time;
                System.out.printf("sent record(key=%s value=%s) "
                        + "meta(partition=%d, offset=%d) time=%d\n",
                        record.key(), record.value(), metadata.partition(),
                        metadata.offset(), elapsedTime);

            }
        } finally {
            producer.flush();
            producer.close();
        }
    }

    public static void main(String... args) throws Exception {
        if (args.length == 0) {
            runProducer(5);
        } else {
            runProducer(Integer.parseInt(args[0]));
        }

    }
}
